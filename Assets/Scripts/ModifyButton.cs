﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifyButton : MonoBehaviour {

    public GameObject parentOfButtonsModify;
    bool isTypes = false;
    Vector3[] startPos;
    Vector3[] endPos;
    Vector3[] currentPos;
    Vector3[] startScale;
    Vector3[] endScale;
    Vector3[] currentScale;
    [HideInInspector]
    public TweenTransforms[][] tweenTransforms;
    float[] delayPos;
    float[] delayScale;
    float[] durationPos;
    float kDuration;
    [HideInInspector]
    public bool isPlaying = false;
    [HideInInspector]
    public bool isOpenInside = false;
    GameObject currentSubMenu;

    private void Start()
    {
        Initialize();
    }

    private void Update()
    {
        for (int i = 0; i < parentOfButtonsModify.transform.childCount; i++)
        {
            currentPos[i] = parentOfButtonsModify.transform.GetChild(i).gameObject.GetComponent<RectTransform>().transform.position;
            currentScale[i] = parentOfButtonsModify.transform.GetChild(i).gameObject.GetComponent<RectTransform>().localScale;
        }



    }

    public void ModifyMenuButton()
    {
        if (!isPlaying)
        {
            if (!isTypes)
            {
                OpenMenu();
            }

            else
            {
                if (!isOpenInside)
                {
                    CloseMenu();
                }
                /*if (currentSubMenu != null)
                {
                    currentSubMenu.SetActive(false);
                    currentSubMenu = null;
                }*/
            }
        }
    }

    public void CloseMenu()
    {
        for (int i = 0; i < parentOfButtonsModify.transform.childCount; i++)
        {
            tweenTransforms[parentOfButtonsModify.transform.childCount - 1 - i][0].delay = kDuration * i;
            tweenTransforms[i][0].Begin(currentPos[i], startPos[i]);
            tweenTransforms[i][1].Begin(currentScale[i], startScale[i]);
        }

        isTypes = false;
        isPlaying = true;
        Invoke("DisableIsPlaying", tweenTransforms[parentOfButtonsModify.transform.childCount - 1][0].delay + tweenTransforms[parentOfButtonsModify.transform.childCount - 1][0].duration);
    }


    void OpenMenu()
    {
        for (int i = 0; i < parentOfButtonsModify.transform.childCount; i++)
        {
            tweenTransforms[i][0].delay = delayPos[i];
            tweenTransforms[i][1].delay = delayScale[i];

            tweenTransforms[i][0].Begin(currentPos[i], endPos[i]);
            tweenTransforms[i][1].Begin(currentScale[i], endScale[i]);
        }

        isTypes = true;
        isPlaying = true;
        Invoke("DisableIsPlaying", tweenTransforms[parentOfButtonsModify.transform.childCount - 1][1].delay + tweenTransforms[parentOfButtonsModify.transform.childCount - 1][1].duration);

    }

    void DisableIsPlaying()
    {
        isPlaying = false;
    }


    void Initialize()
    {
        float kY;
        kY = Screen.height / 1080f;


        tweenTransforms = new TweenTransforms[parentOfButtonsModify.transform.childCount][];

        startPos = new Vector3[parentOfButtonsModify.transform.childCount];
        endPos = new Vector3[parentOfButtonsModify.transform.childCount];
        currentPos = new Vector3[parentOfButtonsModify.transform.childCount];

        startScale = new Vector3[parentOfButtonsModify.transform.childCount];
        endScale = new Vector3[parentOfButtonsModify.transform.childCount];
        currentScale = new Vector3[parentOfButtonsModify.transform.childCount];

        delayPos = new float[parentOfButtonsModify.transform.childCount];
        delayScale = new float[parentOfButtonsModify.transform.childCount];
        durationPos = new float[parentOfButtonsModify.transform.childCount];

        for (int i = 0; i < parentOfButtonsModify.transform.childCount; i++)
        {
            tweenTransforms[i] = parentOfButtonsModify.transform.GetChild(i).gameObject.GetComponents<TweenTransforms>();
            startPos[i] = parentOfButtonsModify.transform.GetChild(i).transform.position;
            endPos[i] = parentOfButtonsModify.transform.GetChild(i).transform.position - new Vector3(0f, 202f * kY * (i + 1) + 140f * kY);
            currentPos[i] = startPos[i];
            startScale[i] = tweenTransforms[i][1].startingVector;
            endScale[i] = tweenTransforms[i][1].endVector;
            currentScale[i] = startScale[i];

            delayPos[i] = tweenTransforms[i][0].delay;
            delayScale[i] = tweenTransforms[i][1].delay;
            durationPos[i] = tweenTransforms[i][0].duration;
        }

        kDuration = durationPos[0] / 2;
    }

    public void SubMenuButton(GameObject gameObject)
    {
        if (!isPlaying)
        {
            if (currentSubMenu != null)
                if (currentSubMenu != gameObject)
                    currentSubMenu.SetActive(false);
                else
                {
                    currentSubMenu.SetActive(false);
                    currentSubMenu = null;
                    return;
                }
            gameObject.SetActive(true);
            currentSubMenu = gameObject;
        }
    }
}
