﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconsOnClick : MonoBehaviour
{

    [System.Serializable]
    public class Icons
    {
        public Button button;
        public Sprite imageDefault, imageClicked;

        public void SetDefaultImage()
        {
            button.GetComponent<Image>().sprite = imageDefault;
        }

        public void SetClickImage()
        {
            button.GetComponent<Image>().sprite = imageClicked;
        }
    }

    [Header("Catalog:")]
    public Icons catalog;
    public Icons[] subMenu;
    public Icons[] risers;
    public Icons[] bowls;
    public Icons[] shakers;

    [Header("Modify:")]
    public Icons modify;
    public Icons[] subMenuModify;

    [Header("ScreenShot:")]
    public Icons screenshot;

    public Dictionary<string, Icons> iconsDict;
    bool isClicked = false;

    private Button nowClickedButton;
    private Sprite defaultSprite;
    Button bufButton;
    Sprite bufSprite;
    


    private void Start()
    {
        iconsDict = new Dictionary<string, Icons>();
        Init();
    }

    void Init()
    {
        iconsDict.Add(catalog.button.name, catalog);
        for (int i = 0; i < subMenu.Length; i++)
        {
            iconsDict.Add(subMenu[i].button.name, subMenu[i]);
        }

        for (int i = 0; i < risers.Length; i++)
        {
            iconsDict.Add(risers[i].button.name, risers[i]);
        }

        for (int i = 0; i < bowls.Length; i++)
        {
            iconsDict.Add(bowls[i].button.name, bowls[i]);
        }
        for (int i = 0; i < shakers.Length; i++)
        {
            iconsDict.Add(shakers[i].button.name, shakers[i]);
        }
        iconsDict.Add(modify.button.name, modify);
        for (int i = 0; i < subMenu.Length; i++)
        {
            iconsDict.Add(subMenuModify[i].button.name, subMenuModify[i]);
        }
        iconsDict.Add(screenshot.button.name, screenshot);
    }

    public void ClickButtonCatalog(GameObject button)
    {
        string nameButton = button.name;
        iconsDict[nameButton].SetClickImage();
        if (nameButton == catalog.button.name)
        {
            if (nowClickedButton != null && defaultSprite != null)
            {
                SetDefaultImageByClick();
                ClearDefault();
            }
            isClicked = false;
            GetComponent<ModifyButton>().CloseMenu();
            /*if (GetComponent<Buttons>().isTypes)
            {
                screenshot.button.gameObject.SetActive(false);
            }
            else
            {
                screenshot.button.gameObject.SetActive(true);
            }*/
        }
        bufButton = nowClickedButton;
        bufSprite = defaultSprite;
        nowClickedButton = iconsDict[nameButton].button;
        defaultSprite = iconsDict[nameButton].imageDefault;
        Invoke("SetDefaultImageByClick", 0.1f);
        Invoke("ClearDefault", 0.1f);
        Invoke("SetDefaults", 0.1f);
    }


    private void SetDefaultImageByClick()
    {
        nowClickedButton.GetComponent<Image>().sprite = defaultSprite;

    }

    void ClearDefault()
    {
        nowClickedButton = null;
        defaultSprite = null;
    }

    void SetDefaults()
    {
        nowClickedButton = bufButton;
        defaultSprite = bufSprite;
    }

    public void ClickButtonSubMenu(GameObject button)
    {
        string nameButton = button.name;
        if (!GetComponent<Buttons>().isPlaying)
        {
            if (!isClicked)
            {
                iconsDict[nameButton].SetClickImage();
                nowClickedButton = iconsDict[nameButton].button;
                defaultSprite = iconsDict[nameButton].imageDefault;
                isClicked = true;
            }
            else
            {
                if (nowClickedButton == iconsDict[nameButton].button)
                {
                    iconsDict[nameButton].SetDefaultImage();
                    ClearDefault();
                    isClicked = false;
                }
                else
                {
                    SetDefaultImageByClick();
                    iconsDict[nameButton].SetClickImage();
                    nowClickedButton = iconsDict[nameButton].button;
                    defaultSprite = iconsDict[nameButton].imageDefault;
                }

            }
        }
        screenshot.button.gameObject.SetActive(false);    ///////////////////////////////////////
    }

    public void ClickButtonOnceChange(GameObject button)
    {
        if (!GetComponent<ModifyButton>().isPlaying)
        {
            string nameButton = button.name;
            iconsDict[nameButton].SetClickImage();
            bufButton = nowClickedButton;
            bufSprite = defaultSprite;
            nowClickedButton = iconsDict[nameButton].button;
            defaultSprite = iconsDict[nameButton].imageDefault;
            Invoke("SetDefaultImageByClick", 0.1f);
            Invoke("SetDefaults", 0.1f);
        }
    }

    public void ClickButtonType(GameObject button)
    {
        string nameButton = button.name;
        screenshot.button.gameObject.SetActive(true);
        Invoke("CloseCatalog", 0.1f);
        


    }

    void CloseCatalog()
    {
        GetComponent<Buttons>().MenuButton();
        modify.button.gameObject.SetActive(true);
    }

    void CloseModify()
    {
        GetComponent<ModifyButton>().ModifyMenuButton();
    }

    public void ClickTypeModify(Button button)
    {
        if (!GetComponent<ModifyButton>().isPlaying)
        {
            string nameButton = button.name;
            modify.button.GetComponent<Image>().sprite = iconsDict[nameButton].imageDefault;
            modify.imageDefault = iconsDict[nameButton].imageDefault;
            modify.imageClicked = iconsDict[nameButton].imageClicked;
            Invoke("CloseModify", 0.1f);
        }
    }

    public void ScreenShotButton()
    {
        //ScreenCapture.CaptureScreenshot("screen_" + System.DateTime.Now.ToString().Replace('/', '-').Replace(':', '-') + ".png");
        //GetComponent<DemoScript>().OnSaveScreenshotPress();
    }

}
